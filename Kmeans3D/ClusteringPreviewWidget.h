#pragma once

#include <ProEngine3D/ProEngine3D.h>

#include <qopenglwidget.h>

#include "IKMeansSolver.h"
#include "IImageClusterizer.h"

using namespace glm;
using namespace std;

/*!
 * Widget class for comparing image and its clusterized version.
 * Widget clusterizes image in real time to allow real time preview of
 * clusterizing proces.
 * 
 */
class ClusteringPreviewWidget :
	public QOpenGLWidget
{
	Q_OBJECT

private:
	Shader mTextureShader;		/*! Shader object for image rendering. */
	shared_ptr<Texture> mClusteredTexture;	/*! Clusterized image texture. */
	unique_ptr<Texture> mOriginalTexture;	/*! Original image texture. */
	unique_ptr<Mesh> mImageMesh;			/*! Quad mesh for image rendering. */

	shared_ptr<IImageClusterizer> mClusterizer;		/*! Pointer to k-means sovler. */

	vec2 mWidgetSize;	/*! Size of the image preview widget. */
	ivec2 mImageSize;	/*! Size of the image. */

public:
	ClusteringPreviewWidget();
	ClusteringPreviewWidget(QWidget *w);
	~ClusteringPreviewWidget();

	/*!
	 * Initializes widget for rendering.
	 * 
	 */
	void initializeGL() override;
	/*!
	 * Called every time when size of the widget changes.
	 * 
	 * \param w New width of the widget.
	 * \param h New height of the widget.
	 */
	void resizeGL(int w, int h) override;
	/*!
	 * Renders image and its clusterized version.
	 * 
	 */
	void paintGL() override;

	/*!
	 * Sets image texture that will be clustered and shown on widget.
	 * 
	 * \param texture Texture of the input image.
	 */
	void SetImage(Bitmap &bitmap);

	/*!
	 * Sets reference to the k-means solver object that will be used
	 * to clusterize input image.
	 * 
	 * \param solver Reference to k-means solver object.
	 */
	void SetClusterizer(shared_ptr<IKMeansSolver> &solver);

	/*!
	 * Creates and returns Bitmap object with clusterized image.
	 * 
	 * \return Bitmap object with clusterized image.
	 */
	Bitmap GetClusterizedBitmap();
};

