#version 330 core

#define PI 3.141592
#define PI2 (PI*2.0)

struct Centroid {
	vec3 position;
	vec3 color;
};

out vec4 FragColor;

uniform sampler2D uDiffuseTexture;
uniform Centroid uCentroids1[255];
uniform Centroid uCentroids2[255];
uniform int uNumCentroids;

in vec2 vTexCoord;

vec3 rgb2pos(vec3 c)
{
	return c * 2.0 - 1.0;
}

vec3 hsv2pos(vec3 c)
{
	return vec3(
		cos(c.b * PI2 * 255.0/180.0) * c.g,
		c.r * 2.0 - 1.0,
		sin(c.b * PI2 * 255.0/180.0) * c.g
	);
}

void main()
{
	vec3 color = vec3(0.0);

	vec3 tex = texture2D(uDiffuseTexture, vec2(vTexCoord)).rgb;
	vec3 texPos = rgb2pos(tex);

	float min_len = 100000.0;
	for(int i = 0; i < uNumCentroids; i++) {
		float len1 = abs(uCentroids1[i].position.x*texPos.z + uCentroids1[i].position.y*texPos.x + uCentroids1[i].position.z) / sqrt(uCentroids1[i].position.x*uCentroids1[i].position.x + uCentroids1[i].position.y*uCentroids1[i].position.y);

		float len2 = abs(uCentroids2[i].position.x*texPos.z + uCentroids2[i].position.y*texPos.y + uCentroids2[i].position.z) / sqrt(uCentroids2[i].position.x*uCentroids2[i].position.x + uCentroids2[i].position.y*uCentroids2[i].position.y);

		float len = len1 * len1 + len2 * len2;
		if(len < min_len) {
			min_len = len;
			color = uCentroids1[i].color;
		}
	}

	FragColor = vec4(color, 1.0);
} 