#version 330 core

#define PI 3.141592
#define PI2 (PI*2.0)

struct Centroid {
	vec3 position;
	vec3 color;
};

out vec4 FragColor;

uniform sampler2D uDiffuseTexture;
uniform Centroid uCentroids[256];
uniform int uNumCentroids;

in vec2 vTexCoord;

vec3 rgb2pos(vec3 c)
{
	return c * 2.0 - 1.0;
}

vec3 hsv2pos(vec3 c)
{
	return vec3(
		cos(c.b * PI2 * 255.0/180.0) * c.g,
		c.r * 2.0 - 1.0,
		sin(c.b * PI2 * 255.0/180.0) * c.g
	);
}

void main()
{
	vec3 color = vec3(0.0);

	vec3 tex = texture2D(uDiffuseTexture, vec2(vTexCoord)).rgb;
	vec3 texPos = rgb2pos(tex);

	float min_len = length(uCentroids[0].position - texPos);
	color = uCentroids[0].color;
	for(int i = 1; i < uNumCentroids; i++) {
		float len = length(uCentroids[i].position - texPos);
		if(len < min_len) {
			min_len = len;
			color = uCentroids[i].color;
		}
	}

	FragColor = vec4(color, 1.0);
} 