#version 330 core

out vec4 FragColor;

uniform sampler2D uDiffuseTexture;

in vec2 vTexCoord;

void main()
{
	vec3 color = texture2D(uDiffuseTexture, vec2(vTexCoord)).rgb;

	FragColor = vec4(color, 1.0);
} 