#version 330 core

#define ANIMATION	1

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;

uniform mat4 uProjectionViewMatrix;
uniform float uAnimationTime;

out vec2 vTexCoord;
out vec3 vColor;
out vec3 vPosition;

float random (vec3 st) {
    return fract(sin(dot(st.xyz, vec3(12.9898, 78.233, 65.1656)))* 43758.5453123);
}

void main()
{
	vPosition = aPosition;
	vTexCoord = aTexCoord;
	vColor = aColor;

#if ANIMATION == 0
	float t = (1.0-uAnimationTime) * (1.3*aPosition.x + 1.5*aPosition.z + 1.0*aPosition.y);
	vec3 pos = vec3(
		aPosition.x * cos(t) - aPosition.z * sin(t),
		aPosition.y,
		aPosition.x * sin(t) + aPosition.z * cos(t)
	) * uAnimationTime;
#elif ANIMATION == 1
	vec3 pos = (uAnimationTime*4.3333 - uAnimationTime*uAnimationTime*3.3333) * (
		pow(uAnimationTime, 3) * aPosition + (1.0 - pow(uAnimationTime, 3)) * aPosition +
		(1.0 - pow(uAnimationTime, 3)) * vec3(random(aPosition*466), random(aPosition*98), random(aPosition*40)) * 0.3
		);
#endif

    gl_Position = uProjectionViewMatrix * vec4(pos, 1.0);
}