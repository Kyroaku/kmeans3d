#pragma once

#include "3rdparty/ProEngine3D/Camera.h"

/*!
 * Angular camera type.
 * This camera allows to look around center, from specified distance.
 * 
 */
class AngularCamera :
	public Camera
{
private:
	glm::vec3 mAngle = glm::vec3(0.0f); /*! 3 euler angles representing angular position of the camera. */
	float mRadius = 0.0f; /*! Distance of the camera from the origin. */

public:
	AngularCamera();
	~AngularCamera();

	/*!
	 * Sets angular position of the camera.
	 * 
	 * \param angle New angular position.
	 */
	void SetAngle(glm::vec3 angle);
	/*!
	 * Sets distance of the camera from the coordinate system origin.
	 * 
	 * \param radius New distance from origin.
	 */
	void SetRadius(float radius);

	/*!
	 * Returns angular position of the camera.
	 * 
	 * \return Angular position as three euler angles.
	 */
	glm::vec3 GetAngle();
	/*!
	 * Returns distance of the camera from the coordinate system origin.
	 * 
	 * \return Distance from origin
	 */
	float GetRadius();
};

