#include "KMeansVisualizer.h"

bool KMeansVisualizer::Build(std::shared_ptr<IKMeansSolver> solver)
{
	mSolver = std::dynamic_pointer_cast<KMeansSolver>(solver);
	if (!mSolver)
		return false;

	Vertex v;

	std::unique_ptr<Mesh> histogram = std::make_unique<Mesh>();
	std::unique_ptr<Mesh> centroids = std::make_unique<Mesh>();

	for (auto p : mSolver->GetPoints()) {
		Vertex v;
		v.mPosition = glm::vec4(p.mPosition, 1.0f);
		v.mColor = glm::vec4(p.mColor, 1.0f);
		histogram->GetVertices().push_back(v);
	}

	for (auto c : mSolver->GetCentralPoints()) {
		Vertex v;
		v.mPosition = glm::vec4(c.mPosition, 1.0f);
		v.mColor = glm::vec4(c.mColor, 1.0f);
		centroids->GetVertices().push_back(v);
	}

	if (histogram->GetNumVertices() != 0) {
		histogram->SetRenderMode(Mesh::ePoints);
		histogram->Build();
	}
	if (centroids->GetNumVertices() != 0) {
		centroids->SetRenderMode(Mesh::ePoints);
		centroids->Build();
	}

	mDataMesh = move(histogram);
	mCentroidsMesh = move(centroids);

	return true;
}

void KMeansVisualizer::Update()
{
	size_t i = 0;
	for (auto p : mSolver->GetPoints()) {
		Vertex v;
		v.mPosition = glm::vec4(p.mPosition, 1.0f);
		//glm::vec3 c = p.mColor; // <- disable coloring by clusters
		glm::vec3 c = mSolver->GetCentralPoints().at(p.mType).mColor;
		v.mColor = glm::vec4(c, 1.0f);
		mDataMesh->GetVertices().at(i++) = v;
	}
	i = 0;
	for (auto c : mSolver->GetCentralPoints()) {
		Vertex v;
		v.mPosition = glm::vec4(c.mPosition, 1.0f);
		v.mColor = glm::vec4(c.mColor, 1.0f);
		mCentroidsMesh->GetVertices().at(i++) = v;
	}
	if (mDataMesh->GetNumVertices() != 0)
		mDataMesh->UpdateBuild();
	if (mCentroidsMesh->GetNumVertices() != 0)
		mCentroidsMesh->UpdateBuild();
}

void KMeansVisualizer::Render()
{
	glEnable(GL_DEPTH_TEST);
	glPointSize(1.0f);
	glEnable(GL_POINT_SMOOTH);
	if (mDataMesh)
		mDataMesh->Render();

	glPointSize(15.0f);
	glEnable(GL_POINT_SMOOTH);
	if (mCentroidsMesh)
		mCentroidsMesh->Render();
}
