#include "IKMeansSolver.h"


IKMeansSolver::~IKMeansSolver()
{
}

void IKMeansSolver::AddPoint(const IKMeansSolver::Point & p)
{
	mPoints.push_back(p);
}

void IKMeansSolver::SetNumberOfValues(const uint16_t & n, std::string initMethod)
{
	mNumberOfValues = n;
}

uint16_t  IKMeansSolver::GetNumberOfValues() const
{
	return mNumberOfValues;
}

std::vector<IKMeansSolver::Point>& IKMeansSolver::GetPoints()
{
	return mPoints;
}
