#pragma once

#include <3rdparty/ProEngine3D/ProEngine3D.h>

#include "KMeansSolver.h"
#include "ISolverVisualizer.h"

/*!
 * 3D Model object for visualizing k-means clustering proces in three dimensions.
 *
 */
class KMeansVisualizer : public ISolverVisualizer
{
private:
	std::shared_ptr<KMeansSolver> mSolver;	/*! Pointer to the solver that is visualized. */
	std::unique_ptr<Mesh> mDataMesh;		/*! Model for data. */
	std::unique_ptr<Mesh> mCentroidsMesh;	/*! Model for clusters centroids. */

public:
	/*!
	 * Builds 3D model based on the k-means solver object.
	 *
	 * \param solver Reference to k-means solver object.
	 */
	bool Build(std::shared_ptr<IKMeansSolver> solver) override;
	/*!
	 * Updates 3D model that has been created, but k-means solver's state changed.
	 *
	 * \param solver Reference to k-means solver object.
	 */
	void Update() override;
	/*!
	 * Renders the 3D model that represents k-means solver's state.
	 *
	 */
	void Render() override;
};