#pragma once

#include <vector>

#include <3rdparty/glm/glm.hpp>

class ISolverVisualizer;
class IImageClusterizer;

class IKMeansSolver
{
public:
	/*!
	 * Structure represents data point in k-means solver.
	 *
	 */
	struct Point {
		glm::vec3 mPosition;	/*! Value of data as a position in 3D space. */
		glm::vec3 mColor;		/*! Color of the data for visualizing purpose. */
		int32_t mType;			/*! Type of the data specyfing which cluster the point is associated to. */
	};

private:
	std::vector<IKMeansSolver::Point> mPoints;
	uint16_t mNumberOfValues;

public:
	virtual ~IKMeansSolver();

	virtual void AddPoint(const IKMeansSolver::Point &p);

	virtual void SetNumberOfValues(const uint16_t &k, std::string initMethod);

	virtual std::vector<IKMeansSolver::Point> &GetPoints();
	virtual uint16_t GetNumberOfValues() const;

	virtual bool Step() = 0;

	/*!
	 * Creates and returns visuailzer for the specific solver implementation.
	 * 
	 * Must be called with proper OpenGL context.
	 * 
	 * \return Visualizer object or nullptr in case of no visualizer for the specific solver.
	 */
	virtual ISolverVisualizer *CreateVisualizer() = 0;

	virtual IImageClusterizer *CreateClusterizer() = 0;
};

