#include "KRegressionsClusterizer.h"

void KRegressionsClusterizer::Build(std::shared_ptr<IKMeansSolver> solver, glm::ivec2 imageSize)
{
	mImageSize = imageSize;
	mSolver = std::dynamic_pointer_cast<KRegressionsSolver>(solver);

	mImageMesh = MeshBuilder::Plane();

	mClusterShader.Create("Shaders/kregressions_clustering");

	mClusteredTexture = std::make_shared<Texture>(Texture::EType::eTexture2D, imageSize);

	mFrameBuffer.AttachColorBuffer(*mClusteredTexture);
	mFrameBuffer.AttachDepthStencilBuffer(mImageSize);
}

shared_ptr<Texture> KRegressionsClusterizer::Clusterize(Texture * image)
{
	/* Save QT framebuffer state. */
	GLint qtFramebuffer = 0;
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qtFramebuffer);

	mFrameBuffer.Bind();
	glClear(GL_COLOR_BUFFER_BIT);

	mClusterShader.Use();

	if (mSolver) {
		for (size_t i = 0; i < mSolver->mCentralLines1.size(); i++) {
			mClusterShader.Uniform3fv("uCentroids1[" + std::to_string(i) + "].position", &mSolver->mCentralLines1.at(i)[0]);
			mClusterShader.Uniform3fv("uCentroids2[" + std::to_string(i) + "].position", &mSolver->mCentralLines2.at(i)[0]);
			mClusterShader.Uniform3fv("uCentroids1[" + std::to_string(i) + "].color", &mSolver->TempGetColor(i)[0]);
		}
		mClusterShader.Uniform1i("uNumCentroids", mSolver->mCentralLines1.size());
	}

	image->Bind(Material::eDiffuseMap);
	glViewport(0, 0, mImageSize.x, mImageSize.y);
	if (mImageMesh)
		mImageMesh->Render();

	/* Restore QT framebuffer state. */
	glBindFramebuffer(GL_FRAMEBUFFER, qtFramebuffer);

	return mClusteredTexture;
}
