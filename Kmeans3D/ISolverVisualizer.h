#pragma once

#include "3rdparty/ProEngine3D/Texture.h"
#include "3rdparty/ProEngine3D/Mesh.h"

class IKMeansSolver;

class ISolverVisualizer
{
private:
	std::unique_ptr<Mesh> mDataMesh;		/*! Model for data. */

public:
	virtual ~ISolverVisualizer() {}

	virtual bool Build(std::shared_ptr<IKMeansSolver> solver) = 0;

	virtual void Update() = 0;

	virtual void Render() = 0;
};