/********************************************************************************
** Form generated from reading UI file 'Kmeans3D.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KMEANS3D_H
#define UI_KMEANS3D_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "ClusteringPreviewWidget.h"
#include "MainSceneWidget.h"

QT_BEGIN_NAMESPACE

class Ui_Kmeans3DClass
{
public:
    QAction *actionOpen;
    QAction *actionAbout;
    QAction *actionStep;
    QAction *actionGenerate;
    QAction *actionClusterize;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_4;
    MainSceneWidget *mainSceneWidget;
    QLabel *bottomText;
    QToolBar *toolBar;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_5;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QPushButton *saveOriginalImageButton;
    ClusteringPreviewWidget *clusteringPreviewWidget;
    QPushButton *saveImageButton;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_6;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QLabel *label_7;
    QComboBox *solverImplCombo;
    QLabel *label_5;
    QComboBox *clustersInitCombo;
    QLabel *label;
    QSpinBox *numberOfCentroidsNumeric;
    QSlider *numberOfCentroidsSlider;
    QGroupBox *groupBox_3;
    QFormLayout *formLayout_3;
    QLabel *label_2;
    QLabel *label_6;
    QSpinBox *histogramThresholdNumeric;
    QSlider *histogramThresholdSlider;
    QComboBox *colorSpaceComboBox;
    QGroupBox *groupBox_2;
    QFormLayout *formLayout_2;
    QLabel *label_3;
    QSpinBox *numberOfCloudsNumeric;
    QSlider *numberOfCloudsSlider;
    QLabel *label_4;
    QSpinBox *numberOfPointsNumeric;
    QSlider *numberOfPointsSlider;
    QPushButton *rebuildSolverButton;

    void setupUi(QMainWindow *Kmeans3DClass)
    {
        if (Kmeans3DClass->objectName().isEmpty())
            Kmeans3DClass->setObjectName(QString::fromUtf8("Kmeans3DClass"));
        Kmeans3DClass->resize(1019, 647);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Kmeans3DClass->sizePolicy().hasHeightForWidth());
        Kmeans3DClass->setSizePolicy(sizePolicy);
        Kmeans3DClass->setCursor(QCursor(Qt::ArrowCursor));
        Kmeans3DClass->setAutoFillBackground(false);
        actionOpen = new QAction(Kmeans3DClass);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon;
        icon.addFile(QString::fromUtf8("Img/image_ico.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOpen->setIcon(icon);
        actionAbout = new QAction(Kmeans3DClass);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("Img/about_ico.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAbout->setIcon(icon1);
        actionStep = new QAction(Kmeans3DClass);
        actionStep->setObjectName(QString::fromUtf8("actionStep"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("Img/next_ico.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStep->setIcon(icon2);
        actionGenerate = new QAction(Kmeans3DClass);
        actionGenerate->setObjectName(QString::fromUtf8("actionGenerate"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("Img/generate_ico.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionGenerate->setIcon(icon3);
        actionClusterize = new QAction(Kmeans3DClass);
        actionClusterize->setObjectName(QString::fromUtf8("actionClusterize"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8("Img/clusterize_ico.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionClusterize->setIcon(icon4);
        centralWidget = new QWidget(Kmeans3DClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        mainSceneWidget = new MainSceneWidget(centralWidget);
        mainSceneWidget->setObjectName(QString::fromUtf8("mainSceneWidget"));
        sizePolicy.setHeightForWidth(mainSceneWidget->sizePolicy().hasHeightForWidth());
        mainSceneWidget->setSizePolicy(sizePolicy);
        mainSceneWidget->setCursor(QCursor(Qt::OpenHandCursor));
        mainSceneWidget->setMouseTracking(true);
        mainSceneWidget->setAutoFillBackground(false);

        verticalLayout_4->addWidget(mainSceneWidget);

        bottomText = new QLabel(centralWidget);
        bottomText->setObjectName(QString::fromUtf8("bottomText"));

        verticalLayout_4->addWidget(bottomText);

        verticalLayout_4->setStretch(0, 1);

        verticalLayout->addLayout(verticalLayout_4);

        Kmeans3DClass->setCentralWidget(centralWidget);
        toolBar = new QToolBar(Kmeans3DClass);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        toolBar->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(toolBar->sizePolicy().hasHeightForWidth());
        toolBar->setSizePolicy(sizePolicy1);
        toolBar->setMovable(false);
        toolBar->setAllowedAreas(Qt::AllToolBarAreas);
        toolBar->setFloatable(false);
        Kmeans3DClass->addToolBar(Qt::LeftToolBarArea, toolBar);
        dockWidget = new QDockWidget(Kmeans3DClass);
        dockWidget->setObjectName(QString::fromUtf8("dockWidget"));
        sizePolicy.setHeightForWidth(dockWidget->sizePolicy().hasHeightForWidth());
        dockWidget->setSizePolicy(sizePolicy);
        dockWidget->setMinimumSize(QSize(217, 647));
        dockWidget->setBaseSize(QSize(0, 0));
        dockWidget->setLayoutDirection(Qt::LeftToRight);
        dockWidget->setFloating(false);
        dockWidget->setFeatures(QDockWidget::NoDockWidgetFeatures);
        dockWidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        sizePolicy.setHeightForWidth(dockWidgetContents->sizePolicy().hasHeightForWidth());
        dockWidgetContents->setSizePolicy(sizePolicy);
        dockWidgetContents->setMinimumSize(QSize(0, 625));
        dockWidgetContents->setMaximumSize(QSize(9999, 9999));
        dockWidgetContents->setBaseSize(QSize(0, 0));
        verticalLayout_5 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_5->setSpacing(1);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(dockWidgetContents);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setTabPosition(QTabWidget::South);
        tabWidget->setDocumentMode(false);
        tabWidget->setTabBarAutoHide(true);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        saveOriginalImageButton = new QPushButton(tab);
        saveOriginalImageButton->setObjectName(QString::fromUtf8("saveOriginalImageButton"));

        verticalLayout_2->addWidget(saveOriginalImageButton);

        clusteringPreviewWidget = new ClusteringPreviewWidget(tab);
        clusteringPreviewWidget->setObjectName(QString::fromUtf8("clusteringPreviewWidget"));
        clusteringPreviewWidget->setEnabled(true);
        sizePolicy.setHeightForWidth(clusteringPreviewWidget->sizePolicy().hasHeightForWidth());
        clusteringPreviewWidget->setSizePolicy(sizePolicy);
        clusteringPreviewWidget->setMinimumSize(QSize(0, 312));
        clusteringPreviewWidget->setBaseSize(QSize(0, 0));

        verticalLayout_2->addWidget(clusteringPreviewWidget);

        saveImageButton = new QPushButton(tab);
        saveImageButton->setObjectName(QString::fromUtf8("saveImageButton"));

        verticalLayout_2->addWidget(saveImageButton);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_3 = new QVBoxLayout(tab_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, -1);
        scrollArea = new QScrollArea(tab_2);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 209, 589));
        verticalLayout_6 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        groupBox = new QGroupBox(scrollAreaWidgetContents);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy2);
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        groupBox->setFont(font);
        formLayout = new QFormLayout(groupBox);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        QFont font1;
        font1.setBold(false);
        font1.setItalic(true);
        font1.setWeight(50);
        label_7->setFont(font1);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_7);

        solverImplCombo = new QComboBox(groupBox);
        solverImplCombo->setObjectName(QString::fromUtf8("solverImplCombo"));
        QFont font2;
        font2.setBold(false);
        font2.setWeight(50);
        solverImplCombo->setFont(font2);

        formLayout->setWidget(1, QFormLayout::SpanningRole, solverImplCombo);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font1);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_5);

        clustersInitCombo = new QComboBox(groupBox);
        clustersInitCombo->setObjectName(QString::fromUtf8("clustersInitCombo"));
        clustersInitCombo->setFont(font2);

        formLayout->setWidget(3, QFormLayout::SpanningRole, clustersInitCombo);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font1);

        formLayout->setWidget(4, QFormLayout::LabelRole, label);

        numberOfCentroidsNumeric = new QSpinBox(groupBox);
        numberOfCentroidsNumeric->setObjectName(QString::fromUtf8("numberOfCentroidsNumeric"));
        numberOfCentroidsNumeric->setFont(font2);
        numberOfCentroidsNumeric->setMinimum(1);
        numberOfCentroidsNumeric->setMaximum(256);
        numberOfCentroidsNumeric->setValue(4);

        formLayout->setWidget(4, QFormLayout::FieldRole, numberOfCentroidsNumeric);

        numberOfCentroidsSlider = new QSlider(groupBox);
        numberOfCentroidsSlider->setObjectName(QString::fromUtf8("numberOfCentroidsSlider"));
        numberOfCentroidsSlider->setMinimumSize(QSize(0, 20));
        numberOfCentroidsSlider->setMinimum(1);
        numberOfCentroidsSlider->setMaximum(256);
        numberOfCentroidsSlider->setValue(1);
        numberOfCentroidsSlider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(5, QFormLayout::SpanningRole, numberOfCentroidsSlider);


        verticalLayout_6->addWidget(groupBox);

        groupBox_3 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        sizePolicy2.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy2);
        groupBox_3->setFont(font);
        formLayout_3 = new QFormLayout(groupBox_3);
        formLayout_3->setSpacing(6);
        formLayout_3->setContentsMargins(11, 11, 11, 11);
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font1);

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_2);

        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font1);

        formLayout_3->setWidget(3, QFormLayout::LabelRole, label_6);

        histogramThresholdNumeric = new QSpinBox(groupBox_3);
        histogramThresholdNumeric->setObjectName(QString::fromUtf8("histogramThresholdNumeric"));
        histogramThresholdNumeric->setFont(font2);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, histogramThresholdNumeric);

        histogramThresholdSlider = new QSlider(groupBox_3);
        histogramThresholdSlider->setObjectName(QString::fromUtf8("histogramThresholdSlider"));
        histogramThresholdSlider->setMinimumSize(QSize(0, 20));
        histogramThresholdSlider->setOrientation(Qt::Horizontal);

        formLayout_3->setWidget(2, QFormLayout::SpanningRole, histogramThresholdSlider);

        colorSpaceComboBox = new QComboBox(groupBox_3);
        colorSpaceComboBox->setObjectName(QString::fromUtf8("colorSpaceComboBox"));
        colorSpaceComboBox->setFont(font2);
        colorSpaceComboBox->setEditable(false);

        formLayout_3->setWidget(4, QFormLayout::SpanningRole, colorSpaceComboBox);


        verticalLayout_6->addWidget(groupBox_3);

        groupBox_2 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy3);
        groupBox_2->setFont(font);
        groupBox_2->setFlat(false);
        groupBox_2->setCheckable(false);
        formLayout_2 = new QFormLayout(groupBox_2);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_3);

        numberOfCloudsNumeric = new QSpinBox(groupBox_2);
        numberOfCloudsNumeric->setObjectName(QString::fromUtf8("numberOfCloudsNumeric"));
        numberOfCloudsNumeric->setFont(font2);
        numberOfCloudsNumeric->setMinimum(1);
        numberOfCloudsNumeric->setMaximum(20);
        numberOfCloudsNumeric->setValue(4);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, numberOfCloudsNumeric);

        numberOfCloudsSlider = new QSlider(groupBox_2);
        numberOfCloudsSlider->setObjectName(QString::fromUtf8("numberOfCloudsSlider"));
        numberOfCloudsSlider->setMinimum(1);
        numberOfCloudsSlider->setMaximum(20);
        numberOfCloudsSlider->setOrientation(Qt::Horizontal);

        formLayout_2->setWidget(1, QFormLayout::SpanningRole, numberOfCloudsSlider);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_4);

        numberOfPointsNumeric = new QSpinBox(groupBox_2);
        numberOfPointsNumeric->setObjectName(QString::fromUtf8("numberOfPointsNumeric"));
        numberOfPointsNumeric->setFont(font2);
        numberOfPointsNumeric->setMinimum(1);
        numberOfPointsNumeric->setMaximum(999999);
        numberOfPointsNumeric->setValue(5000);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, numberOfPointsNumeric);

        numberOfPointsSlider = new QSlider(groupBox_2);
        numberOfPointsSlider->setObjectName(QString::fromUtf8("numberOfPointsSlider"));
        numberOfPointsSlider->setMinimum(1);
        numberOfPointsSlider->setMaximum(999999);
        numberOfPointsSlider->setOrientation(Qt::Horizontal);

        formLayout_2->setWidget(3, QFormLayout::SpanningRole, numberOfPointsSlider);


        verticalLayout_6->addWidget(groupBox_2);

        rebuildSolverButton = new QPushButton(scrollAreaWidgetContents);
        rebuildSolverButton->setObjectName(QString::fromUtf8("rebuildSolverButton"));
        rebuildSolverButton->setAutoDefault(false);
        rebuildSolverButton->setFlat(false);

        verticalLayout_6->addWidget(rebuildSolverButton);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_3->addWidget(scrollArea);

        tabWidget->addTab(tab_2, QString());

        verticalLayout_5->addWidget(tabWidget);

        dockWidget->setWidget(dockWidgetContents);
        Kmeans3DClass->addDockWidget(static_cast<Qt::DockWidgetArea>(2), dockWidget);

        toolBar->addAction(actionOpen);
        toolBar->addAction(actionGenerate);
        toolBar->addSeparator();
        toolBar->addAction(actionClusterize);
        toolBar->addAction(actionStep);
        toolBar->addSeparator();
        toolBar->addAction(actionAbout);

        retranslateUi(Kmeans3DClass);
        QObject::connect(numberOfCentroidsSlider, SIGNAL(valueChanged(int)), numberOfCentroidsNumeric, SLOT(setValue(int)));
        QObject::connect(numberOfCentroidsNumeric, SIGNAL(valueChanged(int)), numberOfCentroidsSlider, SLOT(setValue(int)));
        QObject::connect(histogramThresholdSlider, SIGNAL(valueChanged(int)), histogramThresholdNumeric, SLOT(setValue(int)));
        QObject::connect(histogramThresholdNumeric, SIGNAL(valueChanged(int)), histogramThresholdSlider, SLOT(setValue(int)));
        QObject::connect(numberOfCloudsNumeric, SIGNAL(valueChanged(int)), numberOfCloudsSlider, SLOT(setValue(int)));
        QObject::connect(numberOfCloudsSlider, SIGNAL(valueChanged(int)), numberOfCloudsNumeric, SLOT(setValue(int)));
        QObject::connect(numberOfPointsNumeric, SIGNAL(valueChanged(int)), numberOfPointsSlider, SLOT(setValue(int)));
        QObject::connect(numberOfPointsSlider, SIGNAL(valueChanged(int)), numberOfPointsNumeric, SLOT(setValue(int)));

        tabWidget->setCurrentIndex(0);
        rebuildSolverButton->setDefault(false);


        QMetaObject::connectSlotsByName(Kmeans3DClass);
    } // setupUi

    void retranslateUi(QMainWindow *Kmeans3DClass)
    {
        Kmeans3DClass->setWindowTitle(QApplication::translate("Kmeans3DClass", "Kmeans3D", nullptr));
        actionOpen->setText(QApplication::translate("Kmeans3DClass", "Open", nullptr));
        actionAbout->setText(QApplication::translate("Kmeans3DClass", "About", nullptr));
#ifndef QT_NO_TOOLTIP
        actionAbout->setToolTip(QApplication::translate("Kmeans3DClass", "About", nullptr));
#endif // QT_NO_TOOLTIP
        actionStep->setText(QApplication::translate("Kmeans3DClass", "Step", nullptr));
        actionGenerate->setText(QApplication::translate("Kmeans3DClass", "Generate", nullptr));
#ifndef QT_NO_TOOLTIP
        actionGenerate->setToolTip(QApplication::translate("Kmeans3DClass", "Generate test histogram", nullptr));
#endif // QT_NO_TOOLTIP
        actionClusterize->setText(QApplication::translate("Kmeans3DClass", "Clusterize", nullptr));
        bottomText->setText(QString());
        toolBar->setWindowTitle(QApplication::translate("Kmeans3DClass", "toolBar", nullptr));
#ifndef QT_NO_ACCESSIBILITY
        dockWidget->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
        saveOriginalImageButton->setText(QApplication::translate("Kmeans3DClass", "Save original image", nullptr));
        saveImageButton->setText(QApplication::translate("Kmeans3DClass", "Save result image", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Kmeans3DClass", "Preview", nullptr));
        groupBox->setTitle(QApplication::translate("Kmeans3DClass", "Solver", nullptr));
        label_7->setText(QApplication::translate("Kmeans3DClass", "Implementation", nullptr));
        label_5->setText(QApplication::translate("Kmeans3DClass", "Clusters initialization", nullptr));
        label->setText(QApplication::translate("Kmeans3DClass", "Number of clusters", nullptr));
        groupBox_3->setTitle(QApplication::translate("Kmeans3DClass", "Image data", nullptr));
#ifndef QT_NO_TOOLTIP
        label_2->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("Kmeans3DClass", "Histogram threshold", nullptr));
        label_6->setText(QApplication::translate("Kmeans3DClass", "Color space", nullptr));
#ifndef QT_NO_TOOLTIP
        histogramThresholdSlider->setToolTip(QApplication::translate("Kmeans3DClass", "<html><head/><body><p>Threshold specifies how many times color have to occur to be considered in k-means solver. Higher value means smaller dataset.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        colorSpaceComboBox->setCurrentText(QString());
        groupBox_2->setTitle(QApplication::translate("Kmeans3DClass", "Synthetic dataset", nullptr));
        label_3->setText(QApplication::translate("Kmeans3DClass", "Number of clouds", nullptr));
        label_4->setText(QApplication::translate("Kmeans3DClass", "Number of points", nullptr));
        rebuildSolverButton->setText(QApplication::translate("Kmeans3DClass", "Rebuild Solver", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("Kmeans3DClass", "Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Kmeans3DClass: public Ui_Kmeans3DClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KMEANS3D_H
