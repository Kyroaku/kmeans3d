#pragma once

#include "config.h"

class Vertex {
public:
	glm::vec4 mPosition;
	glm::vec4 mColor;
	glm::vec3 mTexCoord;
	glm::vec3 mNormal;
	glm::vec3 mTangent;
};