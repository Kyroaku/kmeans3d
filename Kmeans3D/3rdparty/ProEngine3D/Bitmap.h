#pragma once

#include "config.h"

#include <string>
#include <FreeImage/FreeImage.h>

using std::string;

class Bitmap
{
	FIBITMAP *mBitmap;

public:
	PRO_API Bitmap();
	PRO_API Bitmap(string filename);
	PRO_API Bitmap(uint8_t *data, int width, int height, int bpp);
	PRO_API ~Bitmap();

	PRO_API int GetWidth() const;
	PRO_API int GetHeight() const;
	PRO_API int GetBpp() const;
	PRO_API char *GetData() const;
};