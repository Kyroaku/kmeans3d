#pragma once

#include <vector>

#include "Texture.h"

using std::vector;
using std::pair;
using std::shared_ptr;

class Material {
public:
	enum ETextureType {
		eDiffuseMap = 0,
		eNormalMap,
		eSpecularMap,
		eDisplacementMap,
		eAmbientOcclusionMap,
		eCubeMap,
		eShadowMap,
		eShadowCubeMap,

		eCustom1,
		eCustom2,
		eCustom3
	};

public:
	vector<pair<ETextureType, shared_ptr<Texture>>> mTextures;
	string mName;
	float mOpacity = 1.0f;
	float mShininess = 1.0f;
	float mShininessStrength = 1.0f;
	float mReflectivity = 0.0f;
	glm::vec3 mColorAmbient = glm::vec3(1.0f);
	glm::vec3 mColorDiffuse = glm::vec3(1.0f);
	glm::vec3 mColorSpecular = glm::vec3(1.0f);

public:
	PRO_API void AddTexture(ETextureType type, shared_ptr<Texture> texture);
};