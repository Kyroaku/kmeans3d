#pragma once

#include <string>
#include <vector>

#include "config.h"

#include "Bitmap.h"

using std::vector;
using std::pair;
using std::shared_ptr;
using std::make_shared;
using std::make_pair;

class Texture {
	/* Structs */
public:
	enum EType {
		eTexture1D = GL_TEXTURE_1D,
		eTexture2D = GL_TEXTURE_2D,
		eTexture3D = GL_TEXTURE_3D,
		eCubeMap = GL_TEXTURE_CUBE_MAP,
		eTexture2D_Multisample = GL_TEXTURE_2D_MULTISAMPLE
	};

	enum ESide {
		eRight = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		eLeft = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		eTop = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		eBottom = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		eBack = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		eFront = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};

	enum EFormat {
		eRGB = GL_RGB,
		eRGBA = GL_RGBA,
		eDepth = GL_DEPTH_COMPONENT
	};

	enum EDataType {
		eFloat = GL_FLOAT,
		eUByte = GL_UNSIGNED_BYTE
	};

	typedef vector<pair<shared_ptr<Bitmap>, ESide>> BitmapList;
	typedef vector<pair<string, ESide>> PathList;

	/* Public members */
private:
	GLuint mTexture;
	EType mTextureType;

	/* Public methods */
public:
	PRO_API Texture();
	PRO_API Texture(EType textureType, glm::vec2 size, EFormat format = eRGBA, EDataType dataType = eUByte, size_t samples = 0);
	PRO_API Texture(const Bitmap &bitmap, EType textureType = Texture::eTexture2D);
	PRO_API Texture(const string &path, EType textureType = Texture::eTexture2D);
	PRO_API Texture(const BitmapList &bitmaps, EType textureType = Texture::eCubeMap);
	PRO_API Texture(const PathList &pathes, EType textureType = Texture::eCubeMap);
	PRO_API ~Texture();

	PRO_API void Create(EType textureType, glm::vec2 size, EFormat format = eRGBA, EDataType dataType = eUByte, GLsizei samples = 0);
	PRO_API void Create(const Bitmap &bitmap, EType textureType = Texture::eTexture2D);
	PRO_API void Create(const string &path, EType textureType = Texture::eTexture2D);
	PRO_API void Create(const BitmapList &bitmaps, EType = Texture::eCubeMap);
	PRO_API void Create(const PathList &pathes, EType = Texture::eCubeMap);

	PRO_API void Bind(int id = 0);

	PRO_API void SetTextureType(EType textureType);

	PRO_API GLuint GetHandle() const;
	PRO_API EType GetTextureType() const;

	/* Private methods */
private:
	void GenerateTexture(EFormat format = Texture::eRGBA);

	void CreateTexture2D(glm::ivec2 size, EFormat format = eRGBA, EDataType dataType = eUByte, GLsizei samples = 0);
	void CreateTexture2D_Multisample(glm::ivec2 size, EFormat format = eRGBA, EDataType dataType = eUByte, GLsizei samples = 0);
	void CreateTexture2D(const Bitmap &bitmap);
	void CreateCubeMap(const BitmapList &bitmaps);
	void CreateCubeMap(glm::ivec2 size, EFormat format = eRGBA, EDataType dataType = eUByte);

	GLenum BppToPixelFormat(int bpp);
};