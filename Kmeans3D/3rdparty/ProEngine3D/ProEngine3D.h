#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "config.h"

#include "Shader.h"
#include "Mesh.h"
#include "Camera.h"
#include "Texture.h"
#include "Model.h"
#include "Light.h"
#include "MeshBuilder.h"
#include "FrameBuffer.h"

#pragma comment(lib, "opengl32")
#pragma comment(lib, "glew32")

class ProEngine3D {
public:
	PRO_API static bool Init();
};