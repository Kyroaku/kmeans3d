#pragma once

#include <string>
#include <vector>
#include <memory>

#include "config.h"
#include "Mesh.h"
#include "ModelNode.h"
#include "Material.h"
#include "Shader.h"

using std::string;
using std::vector;
using std::unique_ptr;

struct aiNode;

class Model {
private:
	vector<unique_ptr<Mesh>> mMeshes;
	vector<unique_ptr<Material>> mMaterials;
	shared_ptr<ModelNode> mRootNode;

private:
	void LoadNode(aiNode *ainode, shared_ptr<ModelNode> node);

	void RenderNode(shared_ptr<ModelNode> node, Shader *shader, glm::mat4 transformation = glm::mat4(1.0f));

public:
	PRO_API Model();

	PRO_API void LoadFromFile(string path);

	PRO_API void Render(Shader *shader, glm::mat4 transformation = glm::mat4(1.0f));

	PRO_API vector<unique_ptr<Mesh>> &GetMeshes();
	PRO_API vector<unique_ptr<Material>> &GetMaterials();
	PRO_API shared_ptr<ModelNode> GetRootNode();

};