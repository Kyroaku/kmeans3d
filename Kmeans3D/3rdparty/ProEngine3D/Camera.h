#pragma once

#include "config.h"

class Camera {
private:
	glm::vec3 mPosition;
	glm::vec3 mTargetPosition;
	float mFovy, mAspect, mNearPlane, mFarPlane;
	glm::mat4 mViewMatrix, mProjectionMatrix;

public:
	PRO_API Camera();

	PRO_API void SetPerspective(float fovy, float aspect, float near, float far);
	PRO_API void SetOrthogonal(float width, float height, float depth);

	PRO_API glm::mat4 GetMatrix();
	PRO_API glm::mat4 GetViewMatrix();
	PRO_API glm::mat4 GetProjectionMatrix();

	PRO_API void SetPosition(glm::vec3 pos);
	PRO_API void SetLookAt(glm::vec3 pos);

	PRO_API glm::vec3 GetPosition();
	PRO_API glm::vec3 GetLookAt();

private:
	void UpdateViewMatrix();
	void UpdateProjectionMatrix();
};