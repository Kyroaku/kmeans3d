#pragma once

#include "config.h"

#include <vector>
#include <memory>
#include <glm/glm.hpp>

using std::vector;
using std::shared_ptr;

class ModelNode {
private:
	vector<uint32_t> mMeshes;
	glm::mat4 mTransformation = glm::mat4(1.0f);
	glm::mat4 mTextureTransformation = glm::mat4(1.0f);

	vector<shared_ptr<ModelNode>> mChildrens;
	shared_ptr<ModelNode> mParent = nullptr;

public:
	PRO_API vector<uint32_t> &GetMeshes();
	PRO_API vector<shared_ptr<ModelNode>> &GetChildrens();
	PRO_API shared_ptr<ModelNode> GetParent();
	PRO_API glm::mat4 GetTransformation();
	PRO_API glm::mat4 GetTextureTransformation();

	PRO_API void SetParent(shared_ptr<ModelNode> parent);
	PRO_API void SetTransformation(glm::mat4 transformation);
	PRO_API void SetTextureTransformation(glm::mat4 transformation);

	PRO_API void AddMesh(uint32_t mesh);
	PRO_API void AddChildren(shared_ptr<ModelNode> children);
};