#pragma once

#include <vector>
#include <algorithm>

#include <3rdparty/glm/glm.hpp>

#include "IKMeansSolver.h"

class KMeansSolver : public IKMeansSolver
{
public:

private:
	std::vector<IKMeansSolver::Point> mCentralPoints;	/*! vector of centroid points. */

public:
	KMeansSolver();
	~KMeansSolver();

	void AddCentralPoint(const KMeansSolver::Point &p);

	std::vector<KMeansSolver::Point> &GetCentralPoints();

	void SetNumberOfValues(const uint16_t &k, std::string initMethod) override;
	void SetRandomCentroids(int k);
	void SetDiagonalCentroids(int k);
	void SetKmeansPlusPlusCentroids(int k);

	bool Step() override;

	ISolverVisualizer *CreateVisualizer() override;
	IImageClusterizer *CreateClusterizer() override;
};
