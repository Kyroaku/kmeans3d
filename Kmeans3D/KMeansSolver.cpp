#include "KMeansSolver.h"

#include <time.h>
#include <random>

#include <qmessagebox.h>

#include "KMeansVisualizer.h"
#include "KMeansClusterizer.h"


KMeansSolver::KMeansSolver()
{
}


KMeansSolver::~KMeansSolver()
{
}

void KMeansSolver::AddCentralPoint(const KMeansSolver::Point &p)
{
	mCentralPoints.push_back(p);
}

std::vector<KMeansSolver::Point> &KMeansSolver::GetCentralPoints()
{
	return mCentralPoints;
}

void KMeansSolver::SetNumberOfValues(const uint16_t & k, std::string initMethod)
{
	IKMeansSolver::SetNumberOfValues(k, initMethod);

	if (initMethod == "Random")
		SetRandomCentroids(k);
	else if (initMethod == "Diagonal")
		SetDiagonalCentroids(k);
	else if (initMethod == "k-means++")
		SetKmeansPlusPlusCentroids(k);
	else
		throw std::exception(("Bad initialization method for this solver: " + initMethod +
			"Possible initializations are: Random, Diagonal, k-means++").c_str());
}

void KMeansSolver::SetRandomCentroids(int k)
{
	srand(time(nullptr));
	mCentralPoints.clear();
	for (int i = 0; i < k; i++) {
		auto p = GetPoints().at(rand() % GetPoints().size());
		mCentralPoints.push_back(p);
	}
}

void KMeansSolver::SetDiagonalCentroids(int k)
{
	mCentralPoints.clear();
	for (int i = 0; i < k; i++) {
		Point p;
		p.mPosition = glm::vec3(2.0f * i / (k - 1) - 1.0f);
		p.mColor = glm::vec3((float)i / (k - 1));
		mCentralPoints.push_back(p);
	}
}

void KMeansSolver::SetKmeansPlusPlusCentroids(int k)
{
	srand(time(nullptr));
	mCentralPoints.clear();

	/* Initialize weighted random generator. */
	std::default_random_engine generator;

	const auto &points = GetPoints();

	/* Add initial, random centroid. */
	auto initialCentroid = points.at(rand() % points.size());
	mCentralPoints.push_back(initialCentroid);

	/* Add other centroids. */
	for (int n = 0; n < k - 1; n++) {
		std::vector<float> probabilities;

		/* For every point. */
		for (int point_i = 0; point_i < points.size(); point_i++) {
			/* Find closest centroid. */
			size_t closesCentroid = 0;
			auto minimalDistance = length(mCentralPoints.at(closesCentroid).mPosition - points.at(point_i).mPosition);
			for (int centroid_i = 0; centroid_i < mCentralPoints.size(); centroid_i++) {
				auto distance = length(mCentralPoints.at(centroid_i).mPosition - points.at(point_i).mPosition);
				if (distance < minimalDistance) {
					closesCentroid = centroid_i;
					minimalDistance = distance;
				}
			}

			/* Add points's probability as square of distance from the closest centroid. */
			probabilities.push_back(minimalDistance*minimalDistance);
		}

		std::discrete_distribution<int> distribution(probabilities.begin(), probabilities.end());
		auto newCentroid_i = distribution(generator);
		mCentralPoints.push_back(points.at(newCentroid_i));
	}
}

bool KMeansSolver::Step()
{
	if (mCentralPoints.empty())
		return 0.0f;

	std::vector<std::pair<Point, size_t>> newCentroids(mCentralPoints.size());
	std::vector<float> minDistsToCentroids(mCentralPoints.size(), 100.0f);

	size_t i = 0;
	for (auto &p : GetPoints()) {
		/* Find centroid closest to point p */
		i = 0;
		float min_len = 100.0f;
		for (auto &c : mCentralPoints) {
			glm::vec3 l = p.mPosition - c.mPosition;
			float len = l.x*l.x + l.y*l.y + l.z*l.z;
			if (len < min_len) {
				min_len = len;
				p.mType = i;
			}
			if (len < minDistsToCentroids.at(i)) {
				minDistsToCentroids.at(i) = len;
				newCentroids.at(i).first.mColor = p.mColor;
			}
			i++;
		}

		auto &cent = newCentroids.at(p.mType);
		cent.first.mPosition += p.mPosition;
		cent.second++;
	}

	float eps = 0.0f;
	for (size_t i = 0; i < newCentroids.size(); i++) {
		if (newCentroids.at(i).second > 0)
			newCentroids.at(i).first.mPosition /= newCentroids.at(i).second;

		const auto dist = length(newCentroids.at(i).first.mPosition - mCentralPoints.at(i).mPosition);
		eps = std::max(eps, dist);

		mCentralPoints.at(i) = newCentroids.at(i).first;
	}

	return eps < 0.0001f;
}

ISolverVisualizer * KMeansSolver::CreateVisualizer()
{
	return new KMeansVisualizer();
}

IImageClusterizer * KMeansSolver::CreateClusterizer()
{
	return new KMeansClusterizer();
}
