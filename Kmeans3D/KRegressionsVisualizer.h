#pragma once

#include <3rdparty/ProEngine3D/ProEngine3D.h>

#include "KRegressionsSolver.h"
#include "ISolverVisualizer.h"

/*!
 * 3D Model object for visualizing k-regressions clustering proces in three dimensions.
 *
 */
class KRegressionsVisualizer : public ISolverVisualizer
{
private:
	std::shared_ptr<KRegressionsSolver> mSolver;	/*! Pointer to the solver that is visualized. */
	std::unique_ptr<Mesh> mDataMesh;				/*! Model for data. */
	std::unique_ptr<Mesh> mCentroidsMesh;			/*! Model for clusters centroids. */

public:
	/*!
	 * Builds 3D model based on the k-regressions solver object.
	 *
	 * \param solver Reference to k-regressions solver object.
	 */
	bool Build(std::shared_ptr<IKMeansSolver> solver) override;
	/*!
	 * Updates 3D model that has been created, but k-regressions solver's state changed.
	 *
	 * \param solver Reference to k-regressions solver object.
	 */
	void Update() override;
	/*!
	 * Renders the 3D model that represents k-regressions solver's state.
	 *
	 */
	void Render() override;
};