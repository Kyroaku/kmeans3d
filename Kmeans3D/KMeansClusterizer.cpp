#include "KMeansClusterizer.h"

void KMeansClusterizer::Build(std::shared_ptr<IKMeansSolver> solver, glm::ivec2 imageSize)
{
	mImageSize = imageSize;
	mSolver = std::dynamic_pointer_cast<KMeansSolver>(solver);

	mImageMesh = MeshBuilder::Plane();

	mClusterShader.Create("Shaders/kmeans_clustering");

	mClusteredTexture = std::make_shared<Texture>(Texture::EType::eTexture2D, imageSize);

	mFrameBuffer.AttachColorBuffer(*mClusteredTexture);
	mFrameBuffer.AttachDepthStencilBuffer(mImageSize);
}

shared_ptr<Texture> KMeansClusterizer::Clusterize(Texture * image)
{
	/* Save QT framebuffer state. */
	GLint qtFramebuffer = 0;
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qtFramebuffer);

	mFrameBuffer.Bind();
	glClear(GL_COLOR_BUFFER_BIT);

	mClusterShader.Use();

	if (mSolver) {
		int i = 0;
		for (auto &p : mSolver->GetCentralPoints()) {
			mClusterShader.Uniform3fv("uCentroids[" + std::to_string(i) + "].position", &p.mPosition[0]);
			mClusterShader.Uniform3fv("uCentroids[" + std::to_string(i) + "].color", &p.mColor[0]);
			i++;
		}
		mClusterShader.Uniform1i("uNumCentroids", mSolver->GetCentralPoints().size());
	}

	image->Bind(Material::eDiffuseMap);
	glViewport(0, 0, mImageSize.x, mImageSize.y);
	if (mImageMesh)
		mImageMesh->Render();

	/* Restore QT framebuffer state. */
	glBindFramebuffer(GL_FRAMEBUFFER, qtFramebuffer);

	return mClusteredTexture;
}
