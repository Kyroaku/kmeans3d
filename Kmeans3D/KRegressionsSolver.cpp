#include "KRegressionsSolver.h"

#include "KRegressionsVisualizer.h"
#include "KRegressionsClusterizer.h"

#include <time.h>

void KRegressionsSolver::SetNumberOfValues(const uint16_t & k, std::string initMethod)
{
	IKMeansSolver::SetNumberOfValues(k, initMethod);

	if (initMethod == "Random")
		InitializeRandom(k);
	else
		throw std::exception(("Bad initialization method for this solver: " + initMethod +
			"Possible initializations are: Random").c_str());
}

void KRegressionsSolver::InitializeRandom(const uint16_t & k)
{
	mCentralLines1.clear();
	mCentralLines2.clear();
	srand(time(nullptr));
	for (uint16_t i = 0; i < k; i++) {
		Point p;
		p.mPosition = GetPoints().at(rand() % GetPoints().size()).mPosition / 2.0f + 0.5f;
		AddRegressionLine(p);
		mColors.push_back(p.mPosition / 2.0f + 0.5f);
	}
}

void KRegressionsSolver::AddRegressionLine(const IKMeansSolver::Point & p)
{
	if (p.mPosition.z > 0.0001f) {
		mCentralLines1.push_back(glm::vec3(p.mPosition.x / p.mPosition.z, -1, -1.0f + p.mPosition.x / p.mPosition.z));
		mCentralLines2.push_back(glm::vec3(p.mPosition.y / p.mPosition.z, -1, -1.0f + p.mPosition.y / p.mPosition.z));
	}
	else {
		mCentralLines1.push_back(glm::vec3(9999.0f, -1, -1.0f + 9999.0f));
		mCentralLines2.push_back(glm::vec3(9999.0f, -1, -1.0f + 9999.0f));
	}
}

std::vector<IKMeansSolver::Point>& KRegressionsSolver::GetRegressionLines()
{
	static std::vector<IKMeansSolver::Point> points;
	points.clear();

	for (int i = 0; i < mCentralLines1.size(); i++) {
		IKMeansSolver::Point p;
		p.mColor = TempGetColor(i);

		p.mPosition = glm::vec3(
			mCentralLines1.at(i).x * (-1.0f) + mCentralLines1.at(i).z,
			mCentralLines2.at(i).x * (-1.0f) + mCentralLines2.at(i).z,
			-1.0f
		);
		points.push_back(p);

		p.mPosition = glm::vec3(
			mCentralLines1.at(i).x * (1.0f) + mCentralLines1.at(i).z,
			mCentralLines2.at(i).x * (1.0f) + mCentralLines2.at(i).z,
			1.0f
		);
		points.push_back(p);
	}
	return points;
}

bool KRegressionsSolver::Step()
{
	if (GetPoints().empty() || mCentralLines1.empty())
		return 0.0f;

	std::vector<Point> newCentroids(mCentralLines1.size());
	std::vector<float> minDistsToCentroids(mCentralLines1.size(), 100.0f);

	std::vector<float> S(mCentralLines1.size());
	std::vector<float> Sx(mCentralLines1.size());
	std::vector<float> Sy1(mCentralLines1.size());
	std::vector<float> Sy2(mCentralLines1.size());
	std::vector<float> Sxx(mCentralLines1.size());
	std::vector<float> Syy1(mCentralLines1.size());
	std::vector<float> Syy2(mCentralLines1.size());
	std::vector<float> Sxy1(mCentralLines1.size());
	std::vector<float> Sxy2(mCentralLines1.size());
	std::vector<float> a1(mCentralLines1.size());
	std::vector<float> a2(mCentralLines1.size());
	std::vector<float> b1(mCentralLines1.size());
	std::vector<float> b2(mCentralLines1.size());

	for (auto &c : mColors)
		c = glm::vec3(0.0f);

	size_t i = 0;
	for (auto &p : GetPoints()) {
		/* Find centroid closest to point p */
		i = 0;
		float min_len = 10000.0f;
		for (auto &c : mCentralLines1) {
			float len1 = abs(mCentralLines1.at(i).x*p.mPosition.z + mCentralLines1.at(i).y*p.mPosition.x + mCentralLines1.at(i).z)
				/ sqrt(mCentralLines1.at(i).x*mCentralLines1.at(i).x + mCentralLines1.at(i).y*mCentralLines1.at(i).y);

			float len2 = abs(mCentralLines2.at(i).x*p.mPosition.z + mCentralLines2.at(i).y*p.mPosition.y + mCentralLines2.at(i).z)
				/ sqrt(mCentralLines2.at(i).x*mCentralLines2.at(i).x + mCentralLines2.at(i).y*mCentralLines2.at(i).y);

			float len = (len1 * len1 + len2 * len2);

			/* Find closest centroid to the point */
			if (len < min_len) {
				min_len = len;
				p.mType = i;
			}

			i++;
		}

		mColors[p.mType] += p.mColor;

		/* For the closest centroid found: accumulate position for mean calculating. */
		/* For least squares:
			x -> z
			y1 -> x
			y2 -> y
		*/
		S[p.mType]++;
		Sx[p.mType] += p.mPosition.z;
		Sy1[p.mType] += p.mPosition.x;
		Sy2[p.mType] += p.mPosition.y;
		Sxx[p.mType] += p.mPosition.z * p.mPosition.z;
		Syy1[p.mType] += p.mPosition.x * p.mPosition.x;
		Syy2[p.mType] += p.mPosition.y * p.mPosition.y;
		Sxy1[p.mType] += p.mPosition.z * p.mPosition.x;
		Sxy2[p.mType] += p.mPosition.z * p.mPosition.y;

	}

	float eps = 0.0f;
	for (size_t i = 0; i < newCentroids.size(); i++) {
		if (S[i] == 0)
			continue;

		mColors[i] /= S[i];

		float D = S[i] * Sxx[i] - Sx[i] * Sx[i];

		float a1 = (S[i] * Sxy1[i] - Sx[i] * Sy1[i]) / D;
		float a2 = (S[i] * Sxy2[i] - Sx[i] * Sy2[i]) / D;
		float b1 = (Sxx[i] * Sy1[i] - Sx[i] * Sxy1[i]) / D;
		float b2 = (Sxx[i] * Sy2[i] - Sx[i] * Sxy2[i]) / D;

		auto dist = length(mCentralLines1[i] - glm::vec3(a1, -1, b1)) +
			length(mCentralLines2[i] - glm::vec3(a2, -1, b2));

		eps = std::max(eps, dist);

		mCentralLines1[i] = glm::vec3(a1, -1, b1);
		mCentralLines2[i] = glm::vec3(a2, -1, b2);

		mCentralLines1[i].x += (mCentralLines1[i].x / 15.0f) * (float(rand()) / RAND_MAX - 0.5f);
		mCentralLines1[i].z += (mCentralLines1[i].z / 15.0f) * (float(rand()) / RAND_MAX - 0.5f);
		mCentralLines2[i].x += (mCentralLines2[i].x / 15.0f) * (float(rand()) / RAND_MAX - 0.5f);
		mCentralLines2[i].z += (mCentralLines2[i].z / 15.0f) * (float(rand()) / RAND_MAX - 0.5f);
	}

	return eps < 0.05f;
}

glm::vec3 KRegressionsSolver::TempGetColor(uint8_t clusterId)
{
	srand(clusterId * 1234);
	return glm::vec3((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX);
	//return mColors[clusterId];
}

ISolverVisualizer * KRegressionsSolver::CreateVisualizer()
{
	return new KRegressionsVisualizer();
}

IImageClusterizer * KRegressionsSolver::CreateClusterizer()
{
	return new KRegressionsClusterizer();
}