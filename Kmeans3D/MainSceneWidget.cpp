#include "MainSceneWidget.h"

#include <qevent.h>
#include <qcoreapplication.h>

#include <qdebug.h>

MainSceneWidget::MainSceneWidget()
	: QOpenGLWidget()
{
}

MainSceneWidget::MainSceneWidget(QWidget * w)
	: QOpenGLWidget(w)
{
}


MainSceneWidget::~MainSceneWidget()
{
}

void MainSceneWidget::initializeGL()
{
	ProEngine3D::Init();

	glClearColor(0.15f, 0.15f, 0.15f, 1.0f);
	glClearDepth(1.0f);

	glEnable(GL_TEXTURE_2D);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_FRONT);

	mTextureShader.Create("Shaders/mainScene");

	mCamera.SetPosition(glm::vec3(2.0f, 2.0f, 2.0f));
	mCamera.SetLookAt(glm::vec3(0.0f));
	mCamera.SetRadius(3.1f);
	mCamera.SetAngle(vec3(
		-pi<float>() / 24.0f,
		-pi<float>() / 20.0f,
		0.0f
	));

	mVisualizer = nullptr;

	mCube = MeshBuilder::Cube();
	mCube->GetIndices().clear(); /* Hack for rendering quads instead of triangles. */
	for (auto &v : mCube->GetVertices()) {
		v.mColor = v.mPosition * 0.5f + 0.5f;
	}
	/* Rebuild without indices. */
	mCube->Build();
	mCube->SetRenderMode(Mesh::ERenderMode::eQuads);
}

void MainSceneWidget::resizeGL(int w, int h)
{
	glViewport(0, 0, w, h);
	mCamera.SetPerspective(60.0f, (float)w / h, 0.001f, 30.0f);
}

void MainSceneWidget::paintGL()
{
	static bool animationRestart = false;
	static auto t = std::chrono::system_clock::now();
	static float dt = 0.0f;

	if (animationRestart) {
		t = std::chrono::system_clock::now();
		dt = 0.0f;
		animationRestart = false;
	}

	float step = 3.0f *(1.1f - mAnimationTime) * dt;
	mAnimationTime = std::min(mAnimationTime += step, 1.0f);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);
	mTextureShader.Use();
	mTextureShader.UniformMatrix4fv("uProjectionViewMatrix", false, &mCamera.GetMatrix()[0][0]);
	mTextureShader.Uniform1f("uAnimationTime", mAnimationTime);
	if(mVisualizer)
		mVisualizer->Render();

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	mTextureShader.UniformMatrix4fv("uProjectionViewMatrix", false, &mCamera.GetMatrix()[0][0]);
	mTextureShader.Uniform1f("uAnimationTime", 1);
	glBindTexture(GL_TEXTURE_2D, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	mCube->Render();

	auto t2 = std::chrono::system_clock::now();
	dt = std::chrono::duration<float>(t2 - t).count();
	t = std::chrono::system_clock::now();

	if (mAnimationTime < 1.0f)
		update();
	else
		animationRestart = true;
}

void MainSceneWidget::mouseMoveEvent(QMouseEvent * event)
{
	static ivec2 mousePrev = ivec2(event->x(), event->y());

	if (event->buttons() & Qt::MouseButton::LeftButton) {
		mCamera.SetAngle(mCamera.GetAngle() + vec3(
			-(event->y() - mousePrev.y) / 100.0f,
			-(event->x() - mousePrev.x) / 150.0f,
			0.0f
		));
		update();
	}

	mousePrev = ivec2(event->x(), event->y());
}

void MainSceneWidget::wheelEvent(QWheelEvent * event)
{
	QPoint numPixels = event->pixelDelta();
	QPoint numDegrees = event->angleDelta() / 8;

	if (!numPixels.isNull()) {
		mCamera.SetRadius(mCamera.GetRadius() - numPixels.y());
		update();
	}
	else if (!numDegrees.isNull()) {
		QPoint numSteps = numDegrees / 15;
		mCamera.SetRadius(mCamera.GetRadius() - numSteps.y());
		update();
	}

	event->accept();
}

void MainSceneWidget::SetVisualizer(shared_ptr<IKMeansSolver> solver)
{
	makeCurrent();

	mVisualizer = std::shared_ptr<ISolverVisualizer>(solver->CreateVisualizer());
	if(mVisualizer)
		mVisualizer->Build(solver);

	mAnimationTime = 0.0f;
	update();
}

void MainSceneWidget::Update()
{
	if (!mVisualizer)
		return;

	mVisualizer->Update();
	update();
}