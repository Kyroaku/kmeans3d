#include "AngularCamera.h"



AngularCamera::AngularCamera()
{
}


AngularCamera::~AngularCamera()
{
}

void AngularCamera::SetAngle(glm::vec3 angle)
{
	mAngle = angle;
	glm::mat4 t = glm::mat4(1.0f);
	t = glm::rotate(t, mAngle.y, glm::vec3(0.0f, 1.0f, 0.0f));
	t = glm::rotate(t, mAngle.x, glm::vec3(1.0f, 0.0f, 0.0f));
	Camera::SetPosition(glm::vec3(t * glm::vec4(0.0f, 0.0f, mRadius, 0.0f)));
	Camera::SetLookAt(glm::vec3(0.0f));
}

void AngularCamera::SetRadius(float radius)
{
	mRadius = radius;
	glm::mat4 t = glm::mat4(1.0f);
	t = glm::rotate(t, mAngle.y, glm::vec3(0.0f, 1.0f, 0.0f));
	t = glm::rotate(t, mAngle.x, glm::vec3(1.0f, 0.0f, 0.0f));
	Camera::SetPosition(glm::vec3(t * glm::vec4(0.0f, 0.0f, mRadius, 0.0f)));
	Camera::SetLookAt(glm::vec3(0.0f));
}

glm::vec3 AngularCamera::GetAngle()
{
	return mAngle;
}

float AngularCamera::GetRadius()
{
	return mRadius;
}
