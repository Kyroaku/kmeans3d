#pragma once

#include <ProEngine3D/ProEngine3D.h>

#include <qopenglwidget.h>

#include "AngularCamera.h"
#include "ISolverVisualizer.h"
#include "IKMeansSolver.h"

using namespace glm;
using namespace std;

/*!
 * 3D scene widget for rendering main application part.
 * 
 */
class MainSceneWidget :
	public QOpenGLWidget
{
	Q_OBJECT

private:
	Shader mTextureShader; /*! OpenGL main shader object. */

	AngularCamera mCamera; /*! Main camera for 3D scene. */

	unique_ptr<Mesh> mCube; /*! 3D histogram background model. */
	shared_ptr<ISolverVisualizer> mVisualizer; /*! 3D histogram model. */

	float mAnimationTime = 0.0f; /*! Value in range [0; 1] representing state of the animation. */

public:
	MainSceneWidget();
	MainSceneWidget(QWidget *w);
	~MainSceneWidget();

	/*!
	 * Initializes rendering.
	 * 
	 */
	void initializeGL() override;
	/*!
	 * Called every time size of the widget changes.
	 * 
	 * \param w New width of the widget.
	 * \param h New height of the widget.
	 */
	void resizeGL(int w, int h) override;
	/*!
	 * Renders 3D scene on the widget.
	 * 
	 */
	void paintGL() override;

	/*!
	 * Called every time mouse cursor is moved.
	 * 
	 * \param event Mouse event information.
	 */
	void mouseMoveEvent(QMouseEvent *event) override;
	/*!
	 * Called every time mouse wheel state changes.
	 * 
	 * \param event Mouse wheel event information
	 */
	void wheelEvent(QWheelEvent *event) override;

	/*!
	 * Sets reference to k-means solver object used to clusterize image
	 * and 3D histogram.
	 * 
	 * \param solver Reference to k-means solver object.
	 */
	void SetVisualizer(shared_ptr<IKMeansSolver> visualizer);

	/*!
	 * Updates 3D scene.
	 * Method should be called after k-means solver's state changed.
	 */
	void Update();
};

