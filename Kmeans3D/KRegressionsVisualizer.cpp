#include "KRegressionsVisualizer.h"

bool KRegressionsVisualizer::Build(std::shared_ptr<IKMeansSolver> solver)
{
	mSolver = std::dynamic_pointer_cast<KRegressionsSolver>(solver);
	if (!mSolver)
		return false;

	Vertex v;

	std::unique_ptr<Mesh> histogram = std::make_unique<Mesh>();
	std::unique_ptr<Mesh> regressionLines = std::make_unique<Mesh>();

	for (auto p : mSolver->GetPoints()) {
		Vertex v;
		v.mPosition = glm::vec4(p.mPosition, 1.0f);
		v.mColor = glm::vec4(p.mColor, 1.0f);
		histogram->GetVertices().push_back(v);
	}

	for (auto c : mSolver->GetRegressionLines()) {
		Vertex v;
		v.mPosition = glm::vec4(c.mPosition, 1.0f);
		v.mColor = glm::vec4(c.mColor, 1.0f);
		regressionLines->GetVertices().push_back(v);
	}

	if (histogram->GetNumVertices() != 0) {
		histogram->SetRenderMode(Mesh::ePoints);
		histogram->Build();
	}
	if (regressionLines->GetNumVertices() != 0) {
		regressionLines->SetRenderMode(Mesh::eLines);
		regressionLines->Build();
	}

	mDataMesh = move(histogram);
	mCentroidsMesh = move(regressionLines);

	return true;
}

void KRegressionsVisualizer::Update()
{
	size_t i = 0;
	for (auto c : mSolver->GetRegressionLines()) {
		Vertex v;
		v.mPosition = glm::vec4(c.mPosition, 1.0f);
		v.mColor = glm::vec4(c.mColor, 1.0f);
		mCentroidsMesh->GetVertices().at(i++) = v;
	}

	i = 0;
	for (auto p : mSolver->GetPoints()) {
		Vertex v;

		v.mPosition = glm::vec4(p.mPosition, 1.0f);

		//glm::vec3 c = p.mColor; // <- disable coloring by clusters
		//TODO: coloring based on regression lines.
		glm::vec3 c = mSolver->TempGetColor(p.mType);
		v.mColor = glm::vec4(c, 1.0f);

		mDataMesh->GetVertices().at(i++) = v;
	}

	if (mDataMesh->GetNumVertices() != 0)
		mDataMesh->UpdateBuild();
	if (mCentroidsMesh->GetNumVertices() != 0)
		mCentroidsMesh->UpdateBuild();
}

void KRegressionsVisualizer::Render()
{
	glEnable(GL_DEPTH_TEST);
	glPointSize(1.0f);
	glEnable(GL_POINT_SMOOTH);
	if (mDataMesh)
		mDataMesh->Render();

	glPointSize(15.0f);
	if (mCentroidsMesh)
		mCentroidsMesh->Render();
}