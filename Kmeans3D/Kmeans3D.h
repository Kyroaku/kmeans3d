#pragma once

#include <3rdparty/ProEngine3D/ProEngine3D.h>

#include <QtWidgets/QMainWindow>
#include "ui_Kmeans3D.h"
#include "ui_AboutDialog.h"

#include <3rdparty/opencv2/opencv.hpp>

/*!
 * Main class for application.
 * 
 */
class Kmeans3D : public QMainWindow
{
	Q_OBJECT

public:
	Kmeans3D(QWidget *parent = Q_NULLPTR);

private:
	Ui::Kmeans3DClass ui;

	shared_ptr<IKMeansSolver> mSolver; /*! k-means solver object. */
	cv::Mat mCurrentImage; /*! Currently set image for processing. */

public slots:
	void on_actionOpen_triggered();
	void on_actionAbout_triggered();
	void on_actionClusterize_triggered();
	void on_actionStep_triggered();
	void on_actionGenerate_triggered();
	void on_saveImageButton_triggered();
	void on_saveOriginalImageButton_triggered();
	void on_currentIndexChanged(const QString &impl);
	void on_rebuildSolverButton_triggered();

private:
	void LateInit();
	void RebuildSolver();
	void BuildSolver(cv::Mat &img, string colorSpace, string initMethod);
	/*!
	 * Generates data for k-means solver based on RGB image histogram.
	 * Threshold can be applied to cut colors that occurs very rarely.
	 * 
	 * \param img Input RGB image.
	 * \param solver k-means solver object.
	 * \param threshold Threshold for image histogram.
	 */
	void BuildSolverRGB(cv::Mat &img, IKMeansSolver &solver, float threshold);
	/*!
	 * Generates data for k-means solver based on HSV image histogram.
	 * Threshold can be applied to cut colors that occurs very rarely
	 * 
	 * \param img Input RGB image.
	 * \param solver k-means solver object.
	 * \param threshold Threshold for image histogram.
	 */
	void BuildSolverHSV(cv::Mat &img, IKMeansSolver &solver, float threshold);
	/*!
	 * Generates random data for k-means solver.
	 * Algorithm generates clouds of random colors. This type of dataset is an ideal
	 * scenario for k-means algorithm.
	 * 
	 * \param solver k-means solver object.
	 * \param numCentroids Number of clouds of random points.
	 * \param numDotsPerCentroid Number of random points per cloud.
	 */
	void BuildSolver(IKMeansSolver &solver, int numCentroids, int numDotsPerCentroid);
	/*!
	 * 
	 * 
	 * \param img
	 * \param solver
	 */
	void BuildPlaceholderSolver(cv::Mat &img, IKMeansSolver &solver);
};