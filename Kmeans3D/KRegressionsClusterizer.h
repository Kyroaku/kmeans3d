#pragma once

#include "IImageClusterizer.h"
#include "KRegressionsSolver.h"

class KRegressionsClusterizer :
	public IImageClusterizer
{
private:
	FrameBuffer mFrameBuffer;
	std::shared_ptr<Texture> mClusteredTexture;
	std::unique_ptr<Mesh> mImageMesh;
	Shader mClusterShader;
	std::shared_ptr<KRegressionsSolver> mSolver;
	glm::ivec2 mImageSize;

public:
	void Build(std::shared_ptr<IKMeansSolver> solver, glm::ivec2 imageSize) override;

	shared_ptr<Texture> Clusterize(Texture * image) override;
};

