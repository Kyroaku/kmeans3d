#pragma once

#include "IKMeansSolver.h"

class KRegressionsSolver :
	public IKMeansSolver
{
public:
	std::vector<glm::vec3> mCentralLines1;
	std::vector<glm::vec3> mCentralLines2;
	std::vector<glm::vec3> mColors;

public:
	void SetNumberOfValues(const uint16_t &k, std::string initMethod) override;

	void InitializeRandom(const uint16_t &k);

	void AddRegressionLine(const IKMeansSolver::Point &p);

	std::vector<IKMeansSolver::Point> &GetRegressionLines();

	bool Step() override;

	/*!
	 * Temporary method for returning color for a given cluster id (type).
	 *
	 * \param clusterId Cluster id (type).
	 * \return RGB color.
	 */
	glm::vec3 TempGetColor(uint8_t clusterId);

	ISolverVisualizer *CreateVisualizer() override;
	IImageClusterizer *CreateClusterizer() override;
};

