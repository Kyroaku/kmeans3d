#include "Kmeans3D.h"

#include <qtimer.h>
#include "qfiledialog.h"
#include "qmessagebox.h"

#include "KMeansSolver.h"
#include "KRegressionsSolver.h"

Kmeans3D::Kmeans3D(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	ui.numberOfCentroidsSlider->setValue(ui.numberOfCentroidsNumeric->value());
	ui.numberOfCloudsSlider->setValue(ui.numberOfCloudsNumeric->value());
	ui.numberOfPointsSlider->setValue(ui.numberOfPointsNumeric->value());

	ui.solverImplCombo->addItems({ "K-Means", "K-Regressions" });
	ui.solverImplCombo->setCurrentIndex(0);

	ui.colorSpaceComboBox->addItems({ "RGB", "HSV", "3D Converter" });
	ui.colorSpaceComboBox->setCurrentIndex(0);

	ui.clustersInitCombo->addItems({ "Random", "Diagonal", "k-means++" });
	ui.clustersInitCombo->setCurrentIndex(0);

	/* Connections. */
	connect(ui.saveImageButton, &QPushButton::clicked, this, &Kmeans3D::on_saveImageButton_triggered);
	connect(ui.saveOriginalImageButton, &QPushButton::clicked, this, &Kmeans3D::on_saveOriginalImageButton_triggered);
	connect(ui.solverImplCombo, static_cast<void (QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged), this, &Kmeans3D::on_currentIndexChanged);
	connect(ui.rebuildSolverButton, &QPushButton::clicked, this, &Kmeans3D::on_rebuildSolverButton_triggered);

	mSolver = shared_ptr<IKMeansSolver>(new KMeansSolver());

	QTimer::singleShot(0, this, [this]() {
		this->LateInit();
	});
}

void Kmeans3D::on_actionOpen_triggered()
{
	auto fileName = QFileDialog::getOpenFileName(this,
		tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.jpeg *.jfif)"));

	mCurrentImage = cv::imread(fileName.toStdString(), cv::IMREAD_UNCHANGED);

	RebuildSolver();
}

void Kmeans3D::on_actionAbout_triggered() {
	QDialog *dialog = new QDialog(this);
	Ui::Dialog uiDialog;
	uiDialog.setupUi(dialog);
	dialog->show();
}

void Kmeans3D::on_actionClusterize_triggered()
{
	int i;
	for (i = 0; i < 1000; i++) {
		if (mSolver->Step())
			break;
	}
	ui.bottomText->setText(("Clusterized in " + std::to_string(i) + " iterations").c_str());

	ui.mainSceneWidget->Update();
	ui.clusteringPreviewWidget->update();
}

void Kmeans3D::on_actionStep_triggered()
{
	mSolver->Step();
	ui.mainSceneWidget->Update();
	ui.clusteringPreviewWidget->update();
}

void Kmeans3D::on_actionGenerate_triggered()
{
	auto numberOfClouds = ui.numberOfCloudsNumeric->value();
	auto numberOfPoints = ui.numberOfPointsNumeric->value();
	BuildSolver(*mSolver, numberOfClouds, numberOfPoints);
	ui.mainSceneWidget->SetVisualizer(mSolver);
}

void Kmeans3D::on_saveImageButton_triggered()
{
	QString path = QFileDialog::getSaveFileName(this, "Save Image");
	if (path.isEmpty())
		return;

	Bitmap bitmap = ui.clusteringPreviewWidget->GetClusterizedBitmap();
	cv::Mat mat(bitmap.GetWidth()*bitmap.GetHeight(), 1, CV_8UC4);
	memcpy(mat.data, bitmap.GetData(), bitmap.GetWidth() * bitmap.GetHeight() * 4);
	mat = mat.reshape(0, bitmap.GetHeight());
	cv::imwrite(path.toStdString(), mat);
}

void Kmeans3D::on_saveOriginalImageButton_triggered()
{
	QString path = QFileDialog::getSaveFileName(this, "Save Image");
	if (path.isEmpty())
		return;

	try {
		cv::imwrite(path.toStdString(), mCurrentImage);
	}
	catch (exception e)
	{
	}
}

void Kmeans3D::on_currentIndexChanged(const QString &impl)
{
	if (impl == "K-Means")
		mSolver = shared_ptr<IKMeansSolver>(new KMeansSolver());
	else if (impl == "K-Regressions")
		mSolver = shared_ptr<IKMeansSolver>(new KRegressionsSolver());
	else
		QMessageBox::warning(this, "Unknown solver", "Unknown implementation of solver: " + impl);

	RebuildSolver();
}

void Kmeans3D::on_rebuildSolverButton_triggered()
{
	RebuildSolver();
}

void Kmeans3D::LateInit()
{
	mCurrentImage = cv::imread("Img/Placeholder2.png");
	auto colorSpace = ui.colorSpaceComboBox->currentText().toStdString();
	auto initMethod = ui.clustersInitCombo->currentText().toStdString();
	BuildSolver(mCurrentImage, colorSpace, initMethod);
}

void Kmeans3D::RebuildSolver()
{
	auto colorSpace = ui.colorSpaceComboBox->currentText().toStdString();
	auto initMethod = ui.clustersInitCombo->currentText().toStdString();
	BuildSolver(mCurrentImage, colorSpace, initMethod);
}

void Kmeans3D::BuildSolver(cv::Mat &img, string colorSpace, string initMethod)
{
	if (!mSolver)
		return;

	if (!img.empty()) {
		if (colorSpace == "RGB")
			BuildSolverRGB(img, *mSolver, ui.histogramThresholdSlider->value());
		else if (colorSpace == "HSV")
			BuildSolverHSV(img, *mSolver, ui.histogramThresholdSlider->value());
		else if (colorSpace == "3D Converter")
			BuildPlaceholderSolver(img, *mSolver);
		else
			QMessageBox::warning(this,
				"Unknown color space",
				"Couldn't build solver due to unknown color space: " + QString(colorSpace.c_str())
			);

		mSolver->SetNumberOfValues(ui.numberOfCentroidsSlider->value(), initMethod);

		ui.mainSceneWidget->SetVisualizer(mSolver);

		Bitmap bitmap = Bitmap(img.reshape(0, 1).data, img.cols, img.rows, (int)img.elemSize() * 8);
		ui.clusteringPreviewWidget->SetImage(bitmap);

		ui.clusteringPreviewWidget->SetClusterizer(mSolver);
	}
}

void Kmeans3D::BuildSolverRGB(cv::Mat &img, IKMeansSolver &solver, float threshold)
{
	glm::ivec3 hist_res(256);

	cv::Mat hist;
	cv::calcHist(
		vector<cv::Mat>{img},
		vector<int>{0, 1, 2},
		cv::Mat(),
		hist,
		vector<int>{hist_res.x, hist_res.y, hist_res.z},
		vector<float>{0, 256, 0, 256, 0, 256},
		false);

	solver.GetPoints().clear();

	Vertex v;
	for (int r = 0; r < hist_res.x; r++) {
		for (int g = 0; g < hist_res.y; g++) {
			for (int b = 0; b < hist_res.z; b++) {
				float val = hist.at<float>(r, g, b);

				if (val <= threshold)
					continue;

				KMeansSolver::Point p;
				p.mPosition = glm::vec3(
					2.0f * b / (hist_res.z - 1) - 1.0f,
					2.0f * g / (hist_res.y - 1) - 1.0f,
					2.0f * r / (hist_res.x - 1) - 1.0f);
				p.mColor = glm::vec3(
					(float)b / (hist_res.z - 1),
					(float)g / (hist_res.y - 1),
					(float)r / (hist_res.x - 1));

				//for (int i = 0; i < val; i++)
					solver.AddPoint(p);
			}
		}
	}
}

void Kmeans3D::BuildSolverHSV(cv::Mat &img, IKMeansSolver &solver, float threshold)
{
	cv::Mat hsv;
	cv::cvtColor(img, hsv, cv::COLOR_BGR2HSV);
	cv::cvtColor(img, img, cv::COLOR_BGR2HSV);
	glm::ivec3 hist_res(180, 256, 256);

	cv::Mat hist;
	cv::calcHist(
		vector<cv::Mat>{hsv},
		vector<int>{0, 1, 2},
		cv::Mat(),
		hist,
		vector<int>{hist_res.x, hist_res.y, hist_res.z},
		vector<float>{0, 180, 0, 256, 0, 256},
		false);

	solver.GetPoints().clear();

	Vertex v;
	cv::Mat hsvPixelMat = cv::Mat(1, 1, CV_8UC3);
	for (int h = 0; h < hist_res.x; h++) {
		for (int s = 0; s < hist_res.y; s++) {
			for (int v = 0; v < hist_res.z; v++) {
				float val = hist.at<float>(h, s, v);

				if (val <= threshold)
					continue;

				KMeansSolver::Point p;

				float hue = (float)h / (hist_res.r - 1);
				float saturation = (float)s / (hist_res.g - 1);
				float value = (float)v / (hist_res.b - 1);
				p.mPosition = glm::vec3(
					cos(hue * 2.0f * glm::pi<float>()) * saturation,
					value * 2.0f - 1.0f,
					sin(hue * 2.0f * glm::pi<float>()) * saturation
				);

				hsvPixelMat.data[0] = h;
				hsvPixelMat.data[1] = s;
				hsvPixelMat.data[2] = v;
				cv::cvtColor(hsvPixelMat, hsvPixelMat, cv::COLOR_HSV2RGB);

				p.mColor = glm::vec3(
					(float)hsvPixelMat.data[0] / 255,
					(float)hsvPixelMat.data[1] / 255,
					(float)hsvPixelMat.data[2] / 255);
				solver.AddPoint(p);
			}
		}
	}
}

void Kmeans3D::BuildSolver(IKMeansSolver &solver, int numCentroids, int numDotsPerCentroid)
{
	solver.GetPoints().clear();

	vector<pair<KMeansSolver::Point, float>> centroids;

	for (int i = 0; i < numCentroids; i++) {
		KMeansSolver::Point p;
		p.mPosition = glm::vec3(
			(float)rand() / RAND_MAX * 1.5f - 0.75f,
			(float)rand() / RAND_MAX * 1.5f - 0.75f,
			(float)rand() / RAND_MAX * 1.5f - 0.75f
		);
		centroids.push_back(make_pair(
			p,
			(float)rand() / RAND_MAX * 1.3f + 0.1f)
		);
	}

	Vertex v;
	for (int i = 0; i < numCentroids; i++) {
		for (int j = 0; j < numDotsPerCentroid; j++) {
			KMeansSolver::Point p;
			p.mPosition = glm::clamp(
				centroids.at(i).first.mPosition +
				normalize(vec3(
				(float)rand() / RAND_MAX - 0.5f,
					(float)rand() / RAND_MAX - 0.5f,
					(float)rand() / RAND_MAX - 0.5f
				)) * centroids.at(i).second * (float)rand() / (float)RAND_MAX,
				-1.0f, 1.0f);
			p.mColor = p.mPosition / 2.0f + 0.5f;
			solver.AddPoint(p);
		}
	}

	auto numberOfCentroids = ui.numberOfCentroidsNumeric->value();
	auto initMethod = ui.clustersInitCombo->currentText().toStdString();
	solver.SetNumberOfValues(numberOfCentroids, initMethod);
}

void Kmeans3D::BuildPlaceholderSolver(cv::Mat &img, IKMeansSolver & solver)
{
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {
			auto &color = img.at<cv::Vec3b>(img.rows - 1 - i, j);
			if (color[0] > 250 && color[1] > 250 && color[2] > 250)
				continue;
			color = cv::Vec3b(
				rand() % 26,
				i * 255 / (img.rows - 1),
				j * 255 / (img.cols - 1)
			);
		}
	}
	BuildSolverRGB(img, solver, 0.0f);
}
