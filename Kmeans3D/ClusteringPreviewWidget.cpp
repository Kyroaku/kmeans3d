#include "ClusteringPreviewWidget.h"

ClusteringPreviewWidget::ClusteringPreviewWidget()
	: mOriginalTexture(nullptr)
	, mImageMesh(nullptr)
	, mClusterizer(nullptr)
	, mWidgetSize(vec2(0.0f))
	, mImageSize(ivec2(0))
{
}

ClusteringPreviewWidget::ClusteringPreviewWidget(QWidget * w)
	: ClusteringPreviewWidget()
{
}

ClusteringPreviewWidget::~ClusteringPreviewWidget()
{
}

void ClusteringPreviewWidget::initializeGL()
{
	glEnable(GL_TEXTURE_2D);

	mImageMesh = MeshBuilder::Plane();

	mTextureShader.Create("Shaders/texture");
}

void ClusteringPreviewWidget::resizeGL(int w, int h)
{
	mWidgetSize = vec2(w, h);
}

void ClusteringPreviewWidget::paintGL()
{
	/* Clusterize. */

	if (mClusterizer)
		mClusteredTexture = mClusterizer->Clusterize(mOriginalTexture.get());

	/* Render images. */

	mTextureShader.Use();
	mTextureShader.Uniform1i("uTexture", Material::eDiffuseMap);

	if (mOriginalTexture)
		mOriginalTexture->Bind(Material::eDiffuseMap);
	glViewport(0, mWidgetSize.y / 2, mWidgetSize.x, mWidgetSize.y / 2);
	if (mImageMesh)
		mImageMesh->Render();

	if (mClusteredTexture) {
		mClusteredTexture->Bind(Material::eDiffuseMap);
		glViewport(0, 0, mWidgetSize.x, mWidgetSize.y / 2);
		if (mImageMesh)
			mImageMesh->Render();
	}
}

void ClusteringPreviewWidget::SetImage(Bitmap &bitmap)
{
	mImageSize = ivec2(bitmap.GetWidth(), bitmap.GetHeight());

	/* Generate frmebuffer for texture rendering */
	makeCurrent();

	mOriginalTexture = make_unique<Texture>(bitmap);
	update();
}

void ClusteringPreviewWidget::SetClusterizer(shared_ptr<IKMeansSolver> &solver)
{
	makeCurrent();
	mClusterizer = std::shared_ptr<IImageClusterizer>(solver->CreateClusterizer());
	if(mClusterizer)
		mClusterizer->Build(solver, mImageSize);
}

Bitmap ClusteringPreviewWidget::GetClusterizedBitmap()
{
	const int NUM_CHANNELS = 4;
	const int BITS_PER_CHANNEL = 8;

	makeCurrent();
	mClusteredTexture->Bind();
	unique_ptr<uint8_t[]> data = make_unique<uint8_t[]>(mImageSize.x * mImageSize.y * NUM_CHANNELS);
	glGetTexImage(mClusteredTexture->GetTextureType(), 0, GL_BGRA, Texture::EDataType::eUByte, data.get());
	return Bitmap(data.get(), mImageSize.x, mImageSize.y, NUM_CHANNELS * BITS_PER_CHANNEL);
}
