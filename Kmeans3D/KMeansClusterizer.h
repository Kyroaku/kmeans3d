#pragma once

#include "IImageClusterizer.h"

class KMeansClusterizer :
	public IImageClusterizer
{
private:
	FrameBuffer mFrameBuffer;
	std::shared_ptr<Texture> mClusteredTexture;
	std::unique_ptr<Mesh> mImageMesh;
	Shader mClusterShader;
	std::shared_ptr<KMeansSolver> mSolver;
	glm::ivec2 mImageSize;

public:
	void Build(std::shared_ptr<IKMeansSolver> solver, glm::ivec2 imageSize) override;

	shared_ptr<Texture> Clusterize(Texture * image) override;
};

