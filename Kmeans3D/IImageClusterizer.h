#pragma once

#include <3rdparty/ProEngine3D/ProEngine3D.h>

#include "KMeansSolver.h"

class IImageClusterizer
{
public:
	virtual ~IImageClusterizer() {}

	virtual void Build(std::shared_ptr<IKMeansSolver> solver, glm::ivec2 imagesize) = 0;

	virtual shared_ptr<Texture> Clusterize(Texture *image) = 0;
};